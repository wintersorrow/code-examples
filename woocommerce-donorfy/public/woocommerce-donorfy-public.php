<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.1.0
 * 
 * @package    WooCommerce_Donorfy
 * @subpackage WooCommerce_Donorfy/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    WooCommerce_Donorfy
 * @subpackage WooCommerce_Donorfy/public
 */
class WooCommerce_Donorfy_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $donorfy_api_url, $donorfy_api_key, $donorfy_access_username, $donorfy_access_key, $woocommerce_donations_product_id;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.1.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->donorfy_api_key = self::woocommerce_donorfy_get_api_key();
		$this->donorfy_access_username = self::woocommerce_donorfy_get_username();
		$this->donorfy_access_key = self::woocommerce_donorfy_get_access_key();
		$this->donorfy_excluded_products = self::woocommerce_donorfy_get_excluded_products();
		$this->donorfy_excluded_categories = self::woocommerce_donorfy_get_excluded_categories();

		$this->donorfy_api_url = 'https://data.donorfy.com/api/v1/' . $this->donorfy_api_key;

		$this->woocommerce_donations_product_id = get_option( 'woocommerce_donations_product_id' );

		//On Order Payment Complete trigger Donorfy Import
		add_action( 'woocommerce_payment_complete', array( $this, 'woocommerce_donorfy_init' ) );
	}

	function woocommerce_donorfy_init( $order_id ) {
		self::woocommerce_donorfy_add_order( $order_id );
	}

	function woocommerce_donorfy_get_constituent( $email_address ) {

		$api_call_url = 'https://data.donorfy.com/api/v1/' . $this->donorfy_api_key . '/constituents/EmailAddress/' . $email_address;

		$donorfy_constituent = wp_remote_post( $api_call_url, array(
			'method'      => 'GET',
			'headers'     => array(
				'Content-Type' => 'application/json; charset=utf-8',
				'Authorization' => 'Basic ' . base64_encode( $this->donorfy_access_username . ':' . $this->donorfy_access_key )
			)
		));

		if ( !is_wp_error( $donorfy_constituent ) ) {
			$donorfy_constituent_response_code = wp_remote_retrieve_response_code( $donorfy_constituent );

			//Constituent succesfully found, return constituent ID
			if ($donorfy_constituent_response_code === 200) {
				$donorfy_constituent_response = json_decode($donorfy_constituent['body'])[0];
				return $donorfy_constituent_response->ConstituentId;
			} else {
				return false;
			}
		} else {
			error_log( "woocommerce_donorfy_get_constituent - " . $donorfy_constituent->get_error_message() );
		}

	}

	function woocommerce_donorfy_add_constituent( $constituent ) {

		$api_call_url = $this->donorfy_api_url . '/constituents';

		$donorfy_constituent = wp_remote_post( $api_call_url, array(
			'method'      		=> 'POST',
			'headers'     		=> array(
				'Content-Type' 	=> 'application/json; charset=utf-8',
				'Authorization' => 'Basic ' . base64_encode( $this->donorfy_access_username . ':' . $this->donorfy_access_key )
			),
			'body'		  		=> json_encode( $constituent )
		));

		if ( !is_wp_error( $donorfy_constituent ) ) {
			$donorfy_constituent_response_code = wp_remote_retrieve_response_code( $donorfy_constituent );

			//Constituent succesfully added, return constituent ID
			if ( $donorfy_constituent_response_code === 201 ) {
				$donorfy_constituent_response = json_decode( $donorfy_constituent['body'] );
				return $donorfy_constituent_response->ConstituentId;
			} else {
				return false;
			}
		} else {
			error_log( "woocommerce_donorfy_add_constituent - " . $donorfy_constituent->get_error_message() );
		}
	}

	function woocommerce_donorfy_add_order($order_id) {

		if ( self::woocommerce_donorfy_enabled() && $order_id !== null ) {

			//Grab current order
			$order = new WC_Order( $order_id );
			$donorfy_activity_id = $order->get_meta( 'donorfy_activity_id' );

			//If Donorfy Activity ID already set, exit function and don't duplicate
			if ($donorfy_activity_id !== '') {
				return;
			}

			$current_constituent_id = self::woocommerce_donorfy_get_constituent( $order->get_billing_email() );

			//If customer is not an existing constituent, add them
			if ( !$current_constituent_id ) {

				// Create constituent array
				$constituent = array(
					"Title" 				=> get_post_meta( $order_id, '_billing_title', true ),
					"FirstName" 			=> $order->get_billing_first_name(),
					"LastName" 				=> $order->get_billing_last_name(),
					"AddressLine1" 			=> $order->get_billing_address_1(),
					"AddressLine2" 			=> $order->get_billing_address_2(),
					"Town" 					=> $order->get_billing_city(),
					"County" 				=> $order->get_billing_state(),
					"PostalCode" 			=> $order->get_billing_postcode(),
					"Country" 				=> 'United Kingdom',
					"EmailAddress" 			=> $order->get_billing_email(),
					"Phone1" 				=> $order->get_billing_phone(),
					"ConstituentType"   	=> 'Individual',
					"ChannelPermissions"	=> '22'
				);

				// Add constituent and return ID for adding donations
				$current_constituent_id = self::woocommerce_donorfy_add_constituent( $constituent );

			}

			//Add Acticity to Donorfy
			$activity_api_call_url = $this->donorfy_api_url . '/activities';
			$transaction_api_call_url = $this->donorfy_api_url . '/transactions';

			//Check if WooCommerce Order contains a donation, and need adding as a seperate allocation
			if ( self::woocommerce_donorfy_check_for_donations( $order_id ) ) {

				$donations_total = 0;
				$gift_aid_activity = false;

				// Iterating through each "line" items in the order
				foreach ( $order->get_items() as $item_id => $item_data ) {

					$product = $item_data->get_product();
					$product_id = $product->get_id();

					$item_total = $item_data->get_total();

					if ( $product_id == $this->woocommerce_donations_product_id ) {
						$donations_total += $item_total;
					}
				}

				$transaction = array(
					"Product" 					=> "Donation",
					"Campaign"					=> "General Campaign",
					"Channel" 					=> "Website",
					"PaymentMethod" 			=> "Shop Order",
					"Fund"						=> "General",
					"ExistingConstituentId"		=> $current_constituent_id,
					"DatePaid" 					=> $order->get_date_paid()->date( 'F j, Y, g:i:s A T' ),
					"Amount" 					=> number_format( $donations_total, 2 ),
					"Reference" 				=> "WooCommerce Order - #" . $order->get_order_number()
				);

				if( $order->get_meta('gift_aid_reclaimed') ) {
					$transaction["AddGiftAidDeclaration"] = true;
					$transaction["GiftAidClaimed"] = true;
					$transaction["CanRecoverTax"] = true;

					$address = $order->get_billing_address_1();
					if( $order->get_billing_address_2() )	$address .= ', ' . $order->get_billing_address_2();
					if( $order->get_billing_city() )		$address .= ', ' . $order->get_billing_city();
					if( $order->get_billing_state() )		$address .= ', ' . $order->get_billing_state();
					if( $order->get_billing_postcode() )	$address .= ', ' . $order->get_billing_postcode();
					$address .= ', United Kingdom';

					$gift_aid_activity = array(
						"ActivityType" 				=> "Gift Aid Address",
						"Campaign"					=> "General Campaign",
						"ExistingConstituentId"		=> $current_constituent_id,
						"ActivityDate"				=> $order->get_date_paid()->date('F j, Y, g:i:s A T'),
						"Notes"						=> $address
					);

					$donorfy_activity = wp_remote_post( $activity_api_call_url, array(
						'method'      		=> 'POST',
						'headers'     		=> array(
							'Content-Type' 	=> 'application/json; charset=utf-8',
							'Authorization' => 'Basic ' . base64_encode( $this->donorfy_access_username . ':' . $this->donorfy_access_key )
						),
						'body' 				=> json_encode( $gift_aid_activity )
					));
				}

				$donorfy_transaction = wp_remote_post( $transaction_api_call_url, array(
					'method'      		=> 'POST',
					'headers'     		=> array(
						'Content-Type' 	=> 'application/json; charset=utf-8',
						'Authorization' => 'Basic ' . base64_encode( $this->donorfy_access_username . ':' . $this->donorfy_access_key )
					),
					'body'		 		=> json_encode( $transaction )
				));

				if ( !is_wp_error( $donorfy_transaction ) ) {
					$donorfy_transaction_response_code = wp_remote_retrieve_response_code( $donorfy_transaction );

					if ( $donorfy_transaction_response_code === 201 ) {

						$donorfy_transaction_response = json_decode( $donorfy_transaction['body'] );

						//Save Donorfy transaction ID against WooCommerce Order
						$order->update_meta_data( 'donorfy_transaction_id', $donorfy_transaction_response->Id );
						$order->save();

					} else {
						error_log( "woocommerce_donorfy_add_order - Unable to add transaction as Transaction settings not valid" );
					}
				} else {
					error_log( "woocommerce_donorfy_add_order - " . $donorfy_transaction->get_error_message() );
				}
			}

			$activity = array(
				"ActivityType" 				=> "Resource Order",
				"Campaign"					=> "General Campaign",
				"ExistingConstituentId"		=> $current_constituent_id,
				"ActivityDate"				=> $order->get_date_paid()->date( 'F j, Y, g:i:s A T' ),
			);

			$additional_notes = "Order Reference: #" . $order_id;

			$item_counter = 1;

			// Iterating through each "line" items in the order
			foreach ( $order->get_items() as $item_id => $item_data ) {

				$product = $item_data->get_product();
				$product_id = $product->get_id();

				//If it isn't a donation, add it to the resource order
				if (
					$product_id != $this->woocommerce_donations_product_id
					&& !self::woocommerce_donorfy_check_for_excluded_products( $product_id )
					&& !self::woocommerce_donorfy_check_for_excluded_categories( $product_id )
				) {
					if ( $item_counter == 10 ) {
						$additional_notes .= ' -- Additional Items:';
					}
					if ( $item_counter > 10 ) {
						$additional_notes .= ' ' . self::woocommerce_donorfy_format_product_name( $product_id ) . ' x ' . $item_data->get_quantity() . ', ';
					} else {
						$activity['Code' . $item_counter] 	= self::woocommerce_donorfy_format_product_name( $product_id );
						$activity['Number' . $item_counter] = $item_data->get_quantity();
						$activity['Date' . $item_counter] 	= $order->get_date_paid()->date( 'F j, Y, g:i:s A T' );
					}

					$item_counter++;
				}
			}

			//If products have been added, then do insert into Donorfy
			if( $item_counter > 1 ) {

				$activity['Notes'] = nl2br( htmlentities( $additional_notes ) );

				$donorfy_activity = wp_remote_post( $activity_api_call_url, array(
					'method'      		=> 'POST',
					'headers'     		=> array(
						'Content-Type' 	=> 'application/json; charset=utf-8',
						'Authorization' => 'Basic ' . base64_encode( $this->donorfy_access_username . ':' . $this->donorfy_access_key )
					),
					'body' 				=> json_encode( $activity )
				));

				if ( !is_wp_error( $donorfy_activity ) ) {
					$donorfy_activity_response_code = wp_remote_retrieve_response_code( $donorfy_activity );

					if ( $donorfy_activity_response_code === 201 ) {
						$donorfy_activity_response = json_decode( $donorfy_activity['body'] ;
						//Save Donorfy Activty ID against WooCommerce Order
						$order->update_meta_data( 'donorfy_activity_id', $donorfy_activity_response->Id );
						$order->save();
					}
				} else {
					error_log( "woocommerce_donorfy_add_order - " . $donorfy_activity->get_error_message() );
				}

			}

			return;
		} else {
			error_log( "woocommerce_donorfy_add_order - Unable to add activity as Donorfy API settings not set" );
		}
	}

	//If WooCommerce Order has a donation product return true, else return false
	function woocommerce_donorfy_check_for_donations( $order_id ) {

		$order = new WC_Order( $order_id );
		$items = $order->get_items();

		foreach ( $items as $item ) {
			$product_id = $item['product_id'];
			if ( $product_id == $this->woocommerce_donations_product_id ) {
				return true;
			}
		}

		return false;
	}

	//If Product in WooCommerce Order is in excluded list return true, else return false
	function woocommerce_donorfy_check_for_excluded_products( $product_id) {
		if ( in_array( $product_id, $this->donorfy_excluded_products ) ) {
			return true;
		}
		return false;
	}

	//If Product in WooCommerce Order is in category excluded list return true, else return false
	function woocommerce_donorfy_check_for_excluded_categories( $product_id ) {
		foreach( $this->donorfy_excluded_categories as $excluded_category ) {
			//If product belongs to exclude category return true
			if ( has_term( $excluded_category, 'product_cat', $product_id )  ) {
				return true;
			} else {
				// Else double check its parent if it is a variation production
				$current_product = get_post($product_id);
				if( $current_product->post_parent !== null && $current_product->post_type == 'product_variation' ) {
					if ( has_term( $excluded_category, 'product_cat', $current_product->post_parent )  ) {
						return true;
					}
				}
			}
		}
		return false;
	}

	//Return a 'Donorfy friendly' version of the name so it imports OK.
	function woocommerce_donorfy_format_product_name( $product_id ) {
		$product = get_post( $product_id );
		$product_short_name = get_post_meta( $product_id, 'product_short_name', true );

		if( $product_short_name != null || $product_short_name != '' ) {
			return $product_short_name;
		} else {

			if( strlen( $product->post_title ) > 50 ) {
				$product_short_name = substr( $product->post_title, 0, 40 );
				$product_short_name .= ' - #' . $product_id;

				return $product_short_name;
			} else {
				return $product->post_title;
			}
		}
	}

	function woocommerce_donorfy_enabled() {
		if (
			get_option('woocommerce_donorfy_api_key') != ''
			&& get_option('woocommerce_donorfy_username') != ''
			&& get_option('woocommerce_donorfy_access_key') != ''
		) {
			return true;
		} else return false;
	}

	//Get Plugin Options
	function woocommerce_donorfy_get_api_key() {
		if (get_option('woocommerce_donorfy_api_key') != '') {
			$donorfy_api_key = get_option('woocommerce_donorfy_api_key');
			return $donorfy_api_key;
		}
	}

	function woocommerce_donorfy_get_username() {
		if (get_option('woocommerce_donorfy_username') != '') {
			return get_option('woocommerce_donorfy_username');
		}
	}

	function woocommerce_donorfy_get_access_key() {
		if (get_option('woocommerce_donorfy_access_key') != '') {
			return get_option('woocommerce_donorfy_access_key');
		}
	}

	function woocommerce_donorfy_get_excluded_products() {
		if ( is_array( get_option('woocommerce_donorfy_excluded_products') ) ) {
			return get_option('woocommerce_donorfy_excluded_products');
		} else {
			return array();
		}
	}

	function woocommerce_donorfy_get_excluded_categories() {
		if ( is_array( get_option('woocommerce_donorfy_excluded_categories') ) ) {
			return get_option('woocommerce_donorfy_excluded_categories');
		} else {
			return array();
		}
	}

}