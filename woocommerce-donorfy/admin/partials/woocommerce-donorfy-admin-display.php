<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.1.0
 *
 * @package    WooCommerce_Donorfy
 * @subpackage WooCommerce_Donorfy/admin/partials
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WordPress Settings Page
 */

function woocommerce_donorfy_page() {

    // Check the user capabilities
	if ( !current_user_can( 'manage_woocommerce' ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.', 'woocommerce-donorfy' ) );
	}

    // Save the field values
	if ( isset( $_POST['donorfy_fields_submitted'] ) && $_POST['donorfy_fields_submitted'] == 'submitted' ) {
		foreach ( $_POST as $key => $value ) {
			if ( get_option( $key ) != $value ) {
				update_option( $key, $value );
			} else {
				add_option( $key, $value, '', 'no' );
			}
		}
		if( !isset( $_POST['woocommerce_donorfy_excluded_categories'] ) ) {
			$_POST['woocommerce_donorfy_excluded_categories'] = null;
			update_option( 'woocommerce_donorfy_excluded_categories', null );
		}
		if( !isset( $_POST['woocommerce_donorfy_excluded_products'] ) ) {
			update_option( 'woocommerce_donorfy_excluded_products', null );
		}
	}

	$all_categories = get_terms( 'product_cat' );
	$selected_categories = ( is_array( get_option( 'woocommerce_donorfy_excluded_categories' ) ) ) ? get_option( 'woocommerce_donorfy_excluded_categories' ) : array();

	$all_products = get_posts( array(
		'post_type' 		=> array( 'product', 'product_variation' ),
		'posts_per_page' 	=> -1,
		'post_status' 		=> 'publish',
		'orderby'          	=> 'title',
		'order'          	=> 'ASC',
	));

	$selected_products = ( is_array( get_option( 'woocommerce_donorfy_excluded_products' ) ) ) ? get_option( 'woocommerce_donorfy_excluded_products' ) : array();
?>

<div class="wrap">

	<h2><?php _e( 'WooCommerce - Donorfy Settings', 'woocommerce_donorfy' ); ?></h2>

	<?php if ( isset( $_POST['donorfy_fields_submitted'] ) && $_POST['donorfy_fields_submitted'] == 'submitted' ) { ?>
		<div id="message" class="updated fade"><p><strong><?php _e( 'Your settings have been saved.', 'woocommerce_donorfy' ); ?></strong></p></div>
	<?php } ?>

	<div id="content">

		<form method="post" action="" id="donorfy_settings">
			<input type="hidden" name="donorfy_fields_submitted" value="submitted">
			<div id="poststuff">
				<div class="donorfy_settings">
					<div class="postbox">
						<div class="inside">
						    <h3><?php _e( 'General Settings', 'woocommerce_donorfy' ); ?></h3>
							<table class="form-table">
								<tbody>
									<tr>
										<th>
											<label for="woocommerce_donorfy_api_key"><b><?php _e( 'API Key:', 'woocommerce_donorfy' ); ?></b></label>
										</th>
										<td>
											<input type="password" name="woocommerce_donorfy_api_key" class="regular-text" value="<?php echo get_option( 'woocommerce_donorfy_api_key' ); ?>" /><br />
											<span class="description"><?php echo __( 'Your Donorfy API Key.', 'woocommerce_donorfy' ); ?></span>
										</td>
									</tr>
									<tr>
										<th>
											<label for="woocommerce_donorfy_username"><b><?php _e( 'Username:', 'woocommerce_donorfy' ); ?></b></label>
										</th>
										<td>
											<input type="text" name="woocommerce_donorfy_username" class="regular-text" value="<?php echo get_option( 'woocommerce_donorfy_username' ); ?>" /><br />
											<span class="description"><?php echo __( 'The username can be any name you want to use - this<br>username will be recorded in the change log against<br>operations performed.', 'woocommerce_donorfy' ); ?></span>
										</td>
									</tr>
									<tr>
										<th>
											<label for="woocommerce_donorfy_access_key"><b><?php _e( 'Access Key:', 'woocommerce_donorfy' ); ?></b></label>
										</th>
										<td>
											<input type="password" name="woocommerce_donorfy_access_key" class="regular-text" value="<?php echo get_option( 'woocommerce_donorfy_access_key' ); ?>" /><br />
											<span class="description"><?php echo __( 'Your Donorfy Access Key.', 'woocommerce_donorfy_access_key' ); ?></span>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="2">
											<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e( 'Save Changes', 'woocommerce_donorfy' ); ?>" />
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="postbox">
						<div class="inside">
						    <h3><?php _e( 'Advanced Settings', 'woocommerce_donorfy' ); ?></h3>
							<table class="form-table">
								<tbody>
									<tr>
										<th>
											<label for="woocommerce_donorfy_excluded_categories"><b><?php _e( 'Excluded Categories:', 'woocommerce_donorfy' ); ?></b></label>
										</th>
										<td>
											<select class="donorfy-exclude-categories" name="woocommerce_donorfy_excluded_categories[]" multiple="multiple">
												<?php foreach( $all_categories as $cur_category ) : if( $cur_category->name !== '' ) : ?>
												<option value="<?php echo $cur_category->term_id; ?>" <?php if( in_array( $cur_category->term_id, $selected_categories ) ) echo 'selected="selected"'; ?>><?php echo $cur_category->name; ?></option>
												<?php endif; endforeach; ?>
											</select>
											<br>
											<span class="description"><?php echo __( 'Select any categories you do not wish to be imported into Donorfy.', 'woocommerce_donorfy' ); ?></span>
										</td>
									</tr>
									<tr>
										<th>
											<label for="woocommerce_donorfy_excluded_products"><b><?php _e( 'Excluded Products:', 'woocommerce_donorfy' ); ?></b></label>
										</th>
										<td>
											<select class="donorfy-exclude-products" name="woocommerce_donorfy_excluded_products[]" multiple="multiple">
												<?php foreach( $all_products as $cur_product ) : if( $cur_product->post_title !== '' ) : ?>
												<option value="<?php echo $cur_product->ID; ?>" <?php if( in_array( $cur_product->ID, $selected_products ) ) echo 'selected="selected"'; ?>><?php echo $cur_product->post_title; ?></option>
												<?php endif; endforeach; ?>
											</select>
											<br>
											<span class="description"><?php echo __( 'Select any products you do not wish to be imported into Donorfy.', 'woocommerce_donorfy' ); ?></span>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="2">
											<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e( 'Save Changes', 'woocommerce_donorfy' ); ?>" />
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
				<div class="donorfy_logo">
					<img src="<?php echo plugins_url( 'img/logo.jpeg' , dirname(__FILE__) ); ?>" alt="Donorfy">
				</div>
			</div>
		</form>
	</div>
</div>
<?php } ?>