<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.1.0
 *
 * @package    WooCommerce_Donorfy
 * @subpackage WooCommerce_Donorfy/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WooCommerce_Donorfy
 * @subpackage WooCommerce_Donorfy/admin
 */
class WooCommerce_Donorfy_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.1.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->display_options_page();

		add_action( 'admin_menu', array( $this , 'woocommerce_donorfy_admin_menu' ) );

		//If API details are missing, display warning message
		if( !$this->woocommerce_donorfy_enabled() ) {
			add_action( 'admin_notices', array( $this , 'woocommerce_donorfy_missing_api_details_notice' ) );
		}

		add_filter( 'manage_edit-shop_order_columns',  array( $this , 'woocommerce_donorfy_add_order_column_header' ), 20 );
		add_action( 'manage_shop_order_posts_custom_column', array( $this , 'woocommerce_donorfy_add_order_column_content' ), 20 );

	}

	function woocommerce_donorfy_missing_api_details_notice() { ?>
		<div class="update-nag notice">
			<p><?php _e( 'The WooCommerce Donorfy plugin is enabled, but the Donorfy API details are missing and will not work. <a href="admin.php?page=woocommerce_donorfy">View Details</a>', 'woocommerce-donorfy' ); ?></p>
		</div>
<?php }

	/**
	 * Include partial template files
	 *
	 * @since    1.1.0
	 */
	public function display_options_page() {
		include_once 'partials/woocommerce-donorfy-admin-display.php';
	}

	/**
	 * Adds 'Donorfy' column header to 'Orders' page immediately after 'Status' column.
	 */
	function woocommerce_donorfy_add_order_column_header( $columns ) {

		$new_columns = array();

		foreach ( $columns as $column_name => $column_info ) {

			$new_columns[ $column_name ] = $column_info;

			if ( 'order_status' === $column_name ) {
				$new_columns['order_donorfy'] = __( 'Donorfy', 'woocommerce-donorfy' );
			}
		}

		return $new_columns;
	}

	/**
	 * Adds 'Donorfy' column content to 'Orders' page immediately after 'Status' column.
	 */
	function woocommerce_donorfy_add_order_column_content( $column ) {
		global $post;

		if ( 'order_donorfy' === $column ) {

			$order = wc_get_order( $post->ID );
			$donorfy_transaction_id = $order->get_meta( 'donorfy_transaction_id' );

			if( $donorfy_transaction_id ) {
				echo '<img src="'. plugins_url( 'admin/img/icon-active.png' , dirname(__FILE__) ) .'" alt="Imported into Donorfy" width="30" height="30" />';
			}
			else {
				echo '<img src="'. plugins_url( 'admin/img/icon.png' , dirname(__FILE__) ) .'" alt="Not imported into Donorfy"width="30" height="30" />';
			}
		}
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.1.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-donorfy-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'select2', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), '4.0.7', 'all' );
	}

	/**
	 * Register the scripts for the admin area.
	 *
	 * @since    1.1.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-donorfy-admin.js', array('jquery'), $this->version, true );
		wp_enqueue_script( 'select2', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array('jquery'), '4.0.7', true );
	}

	/**
	 * Register the Donorfy api settings page in WooCommerce submenu
	 *
	 * @since    1.1.0
	 */
	function woocommerce_donorfy_admin_menu() {
		$page = add_submenu_page('woocommerce', __( 'Donorfy', 'woocommerce-donorfy' ), __( 'Donorfy', 'woocommerce-donorfy' ), 'manage_woocommerce', 'woocommerce_donorfy', 'woocommerce_donorfy_page' );
	}

	function woocommerce_donorfy_enabled() {
		if ( 
			get_option( 'woocommerce_donorfy_api_key' ) != ''
			&& get_option( 'woocommerce_donorfy_username' ) != ''
			&& get_option( 'woocommerce_donorfy_access_key' ) != ''
		) {
			return true;
		}
		else return false;
	}

}