(function($) {

    $(document).ready(function() {

        if( $('.donorfy-exclude-categories').length && $.fn.select2 ){ 
            $('.donorfy-exclude-categories').select2();
        }

        if( $('.donorfy-exclude-products').length && $.fn.select2 ){ 
            $('.donorfy-exclude-products').select2();
        }
        
    });

})(jQuery);
