<?php
/*
    Plugin Name: Woocommerce - Donorfy
    Description: This plugin allows you to export donations and WooCommerce orders into Donorfy.
    Version: 2.0.0
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'WOOCOMMERCE_DONORFY_VERSION', '2.0.0' );

require plugin_dir_path( __FILE__ ) . 'includes/woocommerce-donorfy.php';


function run_WooCommerce_Donorfy() {
	$plugin = new WooCommerce_Donorfy();
	$plugin->run();
}

run_WooCommerce_Donorfy();
