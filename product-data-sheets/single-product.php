<?php
require_once __DIR__ . '/vendor/autoload.php';

include_once( 'functions/utilities.php' );

$mpdf = new \Mpdf\Mpdf([
    'mode'      => 'utf-8', 
    'format'    => 'A4', 
    'tempDir'   => __DIR__ . '/downloads'
]);

$stylesheet = file_get_contents( __DIR__ . '/css/style.css' );

$product_name = get_query_var( 'data_sheet_product' );
$product_type = get_query_var( 'data_sheet_product_type' );

$mpdf->WriteHTML( $stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS );
$mpdf->SetHeader( strtoupper( $product_name ) );
$mpdf->SetFooter( '{PAGENO}' );

$product = pds_get_product( $product_name, $product_type, true );

ob_start();

echo $product;

$html = ob_get_contents();
ob_end_clean();

try {
    $mpdf->WriteHTML( $html );
    $mpdf->Output( 'pds-' . $product_name . '.pdf', 'I' );
} catch ( \Mpdf\MpdfException $e ) {
    echo $e->getMessage();
}