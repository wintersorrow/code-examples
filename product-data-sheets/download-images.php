<?php
include_once( 'functions/utilities.php' );

$product_name = get_query_var( 'data_sheet_product' );
$product_type = get_query_var( 'data_sheet_product_type' );

pds_get_product_images( $product_name, $product_type, true );