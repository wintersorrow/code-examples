<?php

function pds_get_product( $product_name, $product_type ) {

    $product = get_page_by_title( $product_name, 'OBJECT', $product_type );

    return pds_format_pdf( $product );
}

function pds_format_pdf( $product ) {

    $html  = pds_format_pdf_header( $product );
    $html .= pds_format_pdf_main_info( $product );
    $html .= pds_format_pdf_variants( $product );
    $html .= pds_format_pdf_specs( $product );
    $html .= pds_format_pdf_footer();

    return $html;
}

function pds_format_pdf_header( $product ) {

    $html = '<html>
                <head>
                    <title>' . $product->post_title . '</title>
                    <meta charset="UTF-8" />
                </head>
                <body>
                    <table class="header-table">
                        <tr><td colspan="2">&nbsp;</td><tr>
                        <tr>
                            <td>
                                <table class="header-table">
                                    <tr>
                                        <td class="product-title" colspan="2">' . $product->post_title . '</td>
                                    </tr>';

    // Add Product Variants                           
    $html .= pds_format_pdf_eans( $product );

                        $html .= '</table>
                            </td>
                            <td align="right"><img src="' . __DIR__ . '/../images/logo.png" width="150" /></td>
                        </tr>
                    </table>';

    return $html;
}

function pds_format_pdf_eans( $product ) {

    $html = '';

    if( $product->post_type === 'tv' ) :
        $product_variants = get_field( 'eans', $product->ID );

        foreach( $product_variants as $product_variant ) :
            $html .=    '<tr>
                            <td>(' . $product_variant['model_number'] . ' - ' . $product_variant['ean'] .')</td>
                        </tr>';
        endforeach;
    
    else: 
        $product_ean = pds_get_ean( $product );

        $html .=    '<tr>
                        <td>('.$product_ean .')</td>
                    </tr>';
    endif;
    
    return $html;
}

function pds_format_pdf_main_info( $product ) {

    $main_image = wp_get_attachment_url( get_field( 'header_thumbnail', $product->ID ) );
    
    $html = '<table class="main-table">
                <tr><td colspan="2">&nbsp;</td><tr>
                <tr>
                    <td><img src="'.$main_image .'" width="340" /></td>
                    <td>
                        <table>
                            <tr>
                                <td><b>'. __( 'General Information', 'pds' ) . '</b></td>
                            </tr>';

    // Add Product Features                           
    $html .= pds_format_pdf_features( $product );

            $html .= '</table>
                    </td>
                </tr>
            </table>';

    return $html;
}

function pds_format_pdf_features( $product ) {

    $html = '';

    $taxonomy = $product->post_type == 'refrigeration' ? 'fridge' : $product->post_type;
    $taxonomy = $taxonomy  . '_features';

    $features = get_terms( array(
        'taxonomy' => $taxonomy,
        'include' => get_field( 'features', $product->ID ),
        'hide_empty' => false,
    ));

    
    foreach( $features as $feature ) :
        $html .= '<tr>
                    <td>' . $feature->name . '</td>
                </tr>';
    endforeach;

    return $html;
}

function pds_format_pdf_variants( $product ) {

    if( $product->post_type === 'tv' ) :
        $product_sizes = get_field( 'eans', $product->ID );

        $html = '<table class="variants-table">
                    <tr>
                        <td colspan="2" class="section-title">' . __( 'Available In:', 'pds' ) . '</td>
                    </tr>';

        $html .= pds_format_pdf_tv_sizes( $product_sizes );

        $html .= '</table>';
    
    else: 

        $html = '<table class="variants-table">';

        $html .= pds_format_pdf_product_variants( $product, true );

        if( pds_format_pdf_product_variants( $product, false ) ) :

        $html .=    '<tr><td colspan="4"></td></tr>
                     <tr>
                        <td colspan="4" class="section-title">' . __( 'Also Available In:', 'pds' ) . '</td>
                     </tr>';

            $html .= pds_format_pdf_product_variants( $product, false );

        endif;

        $html .= '</table>';

    endif;
    
    return $html;
}

function pds_format_pdf_tv_sizes( $product_sizes ) {
    
    $html = '';

    foreach( $product_sizes as $product ) :

        $html .= '<tr>
                    <td>'.$product['model_number'] .'</td>
                    <td>'.$product['size']->name .'</td>
                    <td>'.$product['ean'] .'</td>';
        if( $product['display_energy_efficiency'] ) :
           $html .= '<td>
                        <img src="'. plugin_dir_url( __DIR__ ) . '/images/energy-' . strtolower( $product['energy_efficiency_rating'] ) . '.png" width="48" height="20" />
                    </td>';
        endif;
        $html .= '</tr>';

    endforeach;

    return $html;
}

function pds_format_pdf_product_variants( $product, $main_product = false ) {
    
    $html = '';

    if( $main_product ) :

        $product_ean = '';
        $product_colour = '';

        $eans = get_field( 'eans', $product->ID );

        foreach( $eans as $ean ) {
            if( $ean['model_number'] === $product->post_title ) {
                $product_ean = $ean['ean'];
            }
        }

        $variants = get_field( 'varient_links', $product->ID );

        foreach( $variants as $variant ) {
            if( $variant['product_link'] === $product->ID ) {
                $product_colour = $variant['colour']->name;
            }
        }

        $product_energy_rating_icon = plugin_dir_url( __DIR__ ) . '/images/energy-' . strtolower( get_field( 'energy_efficiency_rating', $product->ID ) ) . '.png';

        $html .= '<tr>
                    <td>'.$product->post_title .'</td>
                    <td>'.$product_colour .'</td>
                    <td>'.$product_ean .'</td>';
        if( get_field( 'display_energy_efficiency', $product->ID ) ) :
           $html .= '<td>
                        <p>' . $product_energy_rating_icon . '</p>
                        <img src="' . $product_energy_rating_icon . '" width="48" height="20" />
                    </td>';
        endif;
        $html .= '</tr>';

    else:

        $product_ean = '';
        $product_colour = '';

        $product_variants = array();

        $eans = get_field( 'eans', $product->ID );

        foreach( $eans as $ean ) :
            if( $ean['model_number'] !== $product->post_title ) :
                $product_variants[$ean['model_number']] = array(
                    'product'   =>  $ean['model_number'],
                    'ean'       =>  $ean['ean']
                );
            endif;
        endforeach;

        $variants = get_field( 'varient_links', $product->ID );

        foreach( $variants as $variant ) :
            $current_product = get_post( $variant['product_link'] );
            if( $variant['product_link'] !== $product->ID ) :
                $product_variants[$current_product->post_title]['colour'] = $variant['colour']->name;

                if( get_field( 'energy_efficiency_rating', $current_product->ID ) ) :
                    $product_variants[$current_product->post_title]['energy_rating'] = get_field( 'energy_efficiency_rating', $current_product->ID );
                endif;
            endif;
        endforeach;

        foreach( $product_variants as $variant ) :
            $html .= '<tr>
                        <td>'.$variant['product'] .'</td>
                        <td>'.$variant['colour'] .'</td>
                        <td>'.$variant['ean'] .'</td>';
            if( $variant['product'] ):
            $html .= '<td>
                            <img src="'. plugin_dir_url( __FILE__ ) . '/images/energy-' . $variant['energy_rating'] . '.png" width="48" height="20" />
                        </td>';
            else:
                $html .= '<td></td>';
            endif;
            $html .= '</tr>';
        endforeach;

    endif;
    
    return $html;
}

function pds_format_pdf_specs( $product ) {

    $html = '';

    if( have_rows( 'group_repeater', $product->ID ) ) : 

        $html .= '<table class="specs-table">
                    <tr>
                        <td colspan="2" class="section-title">' . __( 'Specification', 'pds' ) . '</td>
                    </tr>';

        while ( have_rows( 'group_repeater', $product->ID ) ) : the_row(); $group = get_sub_field( 'group' );

          $html .= '<tr><td colspan="2">&nbsp;</td><tr>
                    <tr>
                        <td><b>' .$group['label'] . '</b></td>
                    </tr>';
                if ( have_rows( $group['value'] ) ) :
                    while ( have_rows( $group['value'] ) ) : the_row(); $attribute = get_sub_field( 'attribute' );
                    $html .= '<tr>';
                            if( $attribute['value'] == 'blank' ) :
                                $html .= '<td>' . get_sub_field( 'value' ) . '</td>';
                            else :
                                $html .= '<td>' . $attribute['label'] . '</td>';
                                if ( get_sub_field('sizes') ) :
                                    if ( have_rows( 'values' ) ) :
                                        $html .= '<td>';
                                        while ( have_rows( 'values' ) ) : the_row(); $term = get_sub_field( 'size' );
                                        $html .= '<b>' . $term->name . ': </b>' . get_sub_field( 'value' ) . '<br>';
                                        endwhile;
                                        $html .= '</td>';
                                    endif;
                                else:
                                    $html .= '<td>' . get_sub_field( 'value' ) . '</td>';
                                endif;
                            endif;
                        $html .= '</tr>';
                    endwhile;
                endif;
        endwhile;

        $html .= '</table>';
    endif;

    return $html;
}

function pds_format_pdf_footer() {
    $html = '</body>
    </html>';

    return $html;
}

function pds_get_ean( $product ){

    $eans = get_field( 'eans', $product->ID );

    if( $eans ){
        foreach( $eans as $ean ) {
            if( $ean['model_number'] === $product->post_title ) {
                return $ean['ean'];
            }
        }
    }

    return false;
}

function pds_get_product_images( $product_name, $product_type ) {

    global $wpdb;

    $zip = new ZipArchive();

    $product = get_page_by_title( $product_name, 'OBJECT', $product_type );

    $attached_images = get_attached_media( 'image', $product->ID );

    $zip_name = $product_name . '_' . time();
    $zip->open( __DIR__ . '/../downloads/' . $zip_name  .'.zip',  ZipArchive::CREATE );

    //Loop through images attached to product
    foreach( $attached_images as $attached_image ) {
        $filepath = get_attached_file( $attached_image->ID );
        if( file_exists( $filepath ) ){
            $zip->addFile( $filepath,  $zip_name . '/' . $attached_image->post_name . '.' . pathinfo( $filepath, PATHINFO_EXTENSION ) );
        }
    }

    $sql = "SELECT meta_value
                FROM {$wpdb->prefix}postmeta
                    WHERE ( 
                        ( {$wpdb->prefix}postmeta.meta_key LIKE '%\_image' )
                    OR 
                        ( {$wpdb->prefix}postmeta.meta_key LIKE '%\_background' )
                    OR 
                        ( {$wpdb->prefix}postmeta.meta_key LIKE '%\_logo' )
                    )
                    AND {$wpdb->prefix}postmeta.meta_key NOT LIKE '\_%'
                    AND {$wpdb->prefix}postmeta.meta_value REGEXP '^-?[0-9]+$'
                    AND {$wpdb->prefix}postmeta.post_id = %d";

    $all_db_images = $wpdb->get_results( $wpdb->prepare( $sql, $product->ID ) );

    //Loop through images attached to product
    foreach( $all_db_images as $db_image ) {
        $filepath = get_attached_file( $db_image->meta_value );
        if( file_exists( $filepath ) ){
            $zip->addFile( $filepath,  $zip_name . '/' . $attached_image->post_name . '.' . pathinfo( $filepath, PATHINFO_EXTENSION ) );
        }
    }

    $zip->close();

    $file_name = basename( $zip_name . '.zip' );

    header( "Content-Type: application/zip" );
    header( "Content-Disposition: attachment; filename=$file_name" );
    header( "Content-Length: " . filesize( getcwd() . '/app/downloads/' . $zip_name . '.zip' ) );

    readfile( __DIR__ . '/../downloads/' . $zip_name . '.zip' );
    exit;
}