<?php
/*
    Plugin Name: Product Data Sheets
    Description: Creates a custom PDF of product data.
    Version: 1.0.0
    Text Domain: pds
    Domain Path: /lang
 */


/**
 * Load plugin textdomain.
 */
add_action( 'init', 'product_data_sheets_load_textdomain' );

function product_data_sheets_load_textdomain() {
    load_plugin_textdomain( 'pds', false, plugin_basename(dirname(__FILE__)) . '/lang' );
}


add_action('init', 'product_data_sheets_custom_rewrite_rule', 10, 0);

function product_data_sheets_custom_rewrite_rule() {
    add_rewrite_tag( '%data_sheet_product%', '([^&]+)' );
    add_rewrite_tag( '%data_sheet_product_type%', '([^&]+)' );
    add_rewrite_rule( '^data-sheets/([^/]*)/([^/]*)/?', 'index.php?data_sheet_product_type=$matches[1]&data_sheet_product=$matches[2]', 'top' );
}


add_action( 'template_redirect', 'prefix_url_rewrite_templates' );

function prefix_url_rewrite_templates() {

    if ( $_GET['download-images'] ) {
        add_filter( 'template_include', function () {
            return __DIR__ . '/download-images.php';
        });
    } else if ( get_query_var( 'data_sheet_product_type' ) && get_query_var( 'data_sheet_product' ) ) {
        add_filter( 'template_include', function () {
            return __DIR__ . '/single-product.php';
        });
    }
}


/**
 * Add links to CMS
 **/

add_action( 'manage_tv_posts_columns', 'pds_tv_column' );

function pds_tv_column( $columns ){
	$new_cols = array( 'product-data' => __( 'Product Data', 'pds' ) );
	return array_merge( $columns, $new_cols );
}


add_action( 'manage_tv_posts_custom_column', 'pds_tv_column_data', 10, 2 );

function pds_tv_column_data( $column, $post_id ){
	switch($column){
		case 'product-data':
			echo '<a href="/data-sheets/tv/'. get_post_field( 'post_name', $post_id ) .'/" target="_blank"><b>' . __( 'Download Product Data', 'pds' ) . '</b></a>';
            echo '<br><a href="/data-sheets/tv/'. get_post_field( 'post_name', $post_id ) .'/?download-images=true" target="_blank"><b>' . __( 'Download Product Images', 'pds' ) . '</b></a>';
			break;
	}
}

add_action( 'manage_refrigeration_posts_columns', 'pds_fridge_column' );

function pds_fridge_column( $columns ){
	$new_cols = array( 'product-data' => __( 'Product Data', 'pds' ) );
	return array_merge( $columns, $new_cols );
}

add_action( 'manage_refrigeration_posts_custom_column', 'pds_fridge_column_data', 10, 2 );

function pds_fridge_column_data( $column, $post_id ){
	switch($column){
		case 'product-data':
			echo '<a href="/data-sheets/refrigeration/'. get_post_field( 'post_name', $post_id ) . '/" target="_blank"><b>' . __( 'Download Product Data', 'pds' ) . '</b></a>';
            echo '<br><a href="/data-sheets/refrigeration/'. get_post_field( 'post_name', $post_id ) . '/?download-images=true" target="_blank"><b>' . __( 'Download Product Images', 'pds' ) . '</b></a>';
			break;
	}
}